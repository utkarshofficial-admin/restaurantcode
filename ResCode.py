inventory=["maggi","fries","drink","chips","pizza"]
print("Our inventory:",inventory)
list=[0]*len(inventory)                                  #creating a null list to store the final menu
total_price=0
num_items=int(input("Enter number of items to buy:"))

if num_items>5:
    print("Invalid input!!")
    exit()

for i in range(num_items):
    string=input("Enter item name:")
    if string!="maggi" or "fries" or "drink" or "chips" or "pizza":         #exception
        print("Invalid item")
        exit()

    qty_item=int(input("Enter quantity:"))
    if string=="maggi":
        price=20*qty_item
        list.append(price)                                    #appending the price value in the list

    if string=="fries":
        price=50*qty_item
        list.append(price)

    if string=="drink":
        price=50*qty_item
        list.append(price)

    if string=="chips":
        print=20*qty_item
        list.append(price)

    if string=="pizza":
        price=200*qty_item
        list.append(price)

for i in range(len(list)):
    total_price+=list[i]
print("Your total:",total_price)                     #summing the price values in the created list